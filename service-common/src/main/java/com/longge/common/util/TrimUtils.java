 package com.longge.common.util;

import com.google.common.base.CharMatcher;

/**
 * @author roger yang
 * @date 6/25/2019
 */
public class TrimUtils {
    /**
     * trim left and right char to "",chars like tab/Chinese space/enter/English space
     * @return
     */
    public static String trimAnyBlank(String str) {
        if(null != str) {
        	// String phone = "‭15868139680"; this string size is 12, index 0 have a char, not English blank or Chinese blank
    		// System.out.println(TrimUtils.trimAnyBlank(phone).length());
            return CharMatcher.anyOf("\r\n\t \u00A0　‭").trimFrom(str);
        }
        return str;
    }
}
